import React, { Component } from 'react'

class Book extends Component {


    state = {
        isEditable: false
    }


    changeKeyHandler = (event) => {
        if(event.key == 'Enter'){
            this.setState({
                isEditable: false
            })
        }
    }

    render() {

        let editable = this.state.isEditable ? <input type='text' onChange={(e) => this.props.changeHandler(e.target.value, this.props.book.id)} onKeyPress={this.changeKeyHandler} value={this.props.book.name} placeholder='Enter Name' /> :
            <p> {this.props.book.name} </p>

        return (
            <li className='list-group-item d-flex'>
                {editable}
                <span className='ml-auto'> {this.props.book.price} </span>
                <a style={{ cursor: 'pointer' }} className='mx-4 btn btn-primary' onClick={() => this.setState({ isEditable: true })}>Edit</a>
                <a style={{ cursor: 'pointer' }} className='btn btn-danger' onClick={() => this.props.deleteHandler(this.props.book.id)}>Delete</a>
            </li>
        )
    }
}

export default Book;