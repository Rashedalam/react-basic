import React, { Component } from 'react'
import Book from './Book/Book'

class Books extends Component{
    render(){
        return(
            <div className='container mt-5'>
                { this.props.books.map(book => {
                    return(
                        <Book changeHandler={ this.props.changeHandler } book={ book } deleteHandler={ this.props.deleteHandler }></Book>
                    )
                }) }
            </div>
        )
    }
}

export default Books