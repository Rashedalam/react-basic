import React, { Component } from 'react'

const initialState = {
    name: '',
    email: '',
    password: '',
    country: '',
    gender: '',
    skills: []
}

class Form extends Component {

    constructor() {
        super()
        this.myForm = React.createRef()
    }

    state = {
        ...initialState
    }

    changeHadler = (event) => {

        if (event.target.type == 'checkbox') {
            if (event.target.checked) {
                this.setState({
                    ...this.state,
                    skills: this.state.skills.concat(event.target.value)
                })
            } else {
                this.setState({
                    ...this.state,
                    skills: this.state.skills.filter(skill => skill != event.target.value)
                })
            }
        } else {
            this.setState({
                [event.target.name]: event.target.value
            })
        }
    }

    submitHandler = (event) => {
        event.preventDefault()
        console.log(this.state)
        this.myForm.current.reset()
        this.setState({
            ...initialState
        })
    }

    render() {
        return (
            <div className='mt-5'>
                <form ref={this.myForm} onSubmit={this.submitHandler}>
                    <div className='form-group'>
                        <label htmlFor='name'>Name:</label>
                        <input type='text' placeholder='Enter Your Name' name='name' className='form-control' value={this.state.name} onChange={this.changeHadler} />
                    </div>
                    <div className='form-group'>
                        <label htmlFor='email'>Email:</label>
                        <input type='email' placeholder='Enter Your Email' name='email' className='form-control' value={this.state.email} onChange={this.changeHadler} />
                    </div>
                    <div className='form-group'>
                        <label htmlFor='password'>Password:</label>
                        <input type='password' placeholder='Enter Your Password' name='password' className='form-control' value={this.state.password} onChange={this.changeHadler} />
                    </div>
                    <div className='form-group'>
                        <label htmlFor='country'>Select Your Country</label>
                        <select name='country' id='country' className='form-control' onChange={this.changeHadler}>
                            <option>Select Your Country</option>
                            <option value='Bangladesh'>Bangladesh</option>
                            <option value='India'>India</option>
                            <option value='Srilanka'>Srilanka</option>
                        </select>
                    </div>

                    <div className='form-group'>
                        <input type='radio' name='gender' id='male' value='Male' onChange={this.changeHadler} />
                        <label htmlFor='male' className='pl-2 pr-2'>Male</label>
                        <input type='radio' name='gender' id='female' value='Female' onChange={this.changeHadler} />
                        <label htmlFor='female' className='pl-2 pr-2'>Female</label>
                    </div>

                    <div className='form-group'>
                        <input type='checkbox' name='skills' id='react' value='React' onChange={this.changeHadler} />
                        <label htmlFor='react' className='pl-2 pr-2'>React</label>
                        <input type='checkbox' name='skills' id='laravel' value='Laravel' onChange={this.changeHadler} />
                        <label htmlFor='laravel' className='pl-2 pr-2'>Laravel</label>
                        <input type='checkbox' name='skills' id='php' value='PHP' onChange={this.changeHadler} />
                        <label htmlFor='php' className='pl-2 pr-2'>PHP</label>
                    </div>

                    <button className='btn btn-success' type='submit'>Submit</button>
                </form>
            </div>
        )
    }
}

export default Form