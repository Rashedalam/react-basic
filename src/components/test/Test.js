import React from 'react';

function Test(props) {
    return (
      <div className="App">
        <h1>My Name is {props.name}</h1>
        <p>I'm a {props.designation}</p>
      </div>
    );
}

export default Test