import React, { Component } from 'react';


class Count extends Component{
    constructor(props) {
        super(props);
        this.state = {
            count: 0,
            color: 'black'
        };
    }

    increase=()=>{
        this.setState({
            count: this.state.count +1
        })
        if (this.state.count >=15){
            this.setState({
                color: 'green'
            })
        }
    }

    decrease =()=>{
        this.setState({
            count: this.state.count -1
        })
        if (this.state.count <= 5){
            this.setState({
                color: 'red'
            })
        }
    }

    render(){
        return(
            <h1 style={{color: this.state.color}}>
                <button onClick={this.increase}>increase</button>
                { this.state.count }
                <button onClick={this.decrease}>decrease</button>
            </h1>
        );
    }
}

export default Count
